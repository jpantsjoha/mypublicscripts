

### Kubernetes Cluster
This alternative example of the NGINX application deployment, is more seucure by design.
This is a basic example, and this could be further worked to specific use cases, but the key here is to demonstrate like-for-like configuration as a code alternative to VM+ansible example.

####First
We create the cluster, (HA, distributed and resilience TBD off topic), where we deploy this example with

Hope this is a self evident selection of features we get with our cluster to be functional

```bash

gcloud beta container --project "jpworkspace" \
clusters create "playground-cluster" \
--zone "us-central1-c" \
--username "admin" \
--cluster-version "1.11.6-gke.6" \
--machine-type "g1-small" \
--image-type "COS" \
--disk-type "pd-standard" \
--disk-size "20" \
--scopes "https://www.googleapis.com/auth/devstorage.read_only",\
"https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring",\
"https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly",\
"https://www.googleapis.com/auth/trace.append" \
--num-nodes "1" \
--no-enable-cloud-logging \
--no-enable-cloud-monitoring \
--no-enable-ip-alias \
--network "projects/jpworkspace/global/networks/default" \
--subnetwork "projects/jpworkspace/regions/us-central1/subnetworks/default" \
--addons HorizontalPodAutoscaling,HttpLoadBalancing \
--enable-autoupgrade \
--metadata disable-legacy-endpoints=true \
--enable-autorepair

```

####Second
We apply the manifects found in the folder with

```
kubect apply -f .

```

Thats it. 

####Expected Outcome

We expect to get the generated service IP (loadbalanced over only 1 instance of such nginx application, and we can have more)

Lets see what we created;

```bash
kubectl -n default get svc,po,configmaps
```

```bash
NAME                             READY     STATUS    RESTARTS   AGE
pod/nginx-app-7c744547b7-ks5wv   1/1       Running   0          2m

NAME                 TYPE           CLUSTER-IP      EXTERNAL-IP     PORT(S)                      AGE
service/kubernetes   ClusterIP      10.47.240.1     <none>          443/TCP                      2m
service/nginx-app    LoadBalancer   10.47.254.166   104.198.24.38   80:30879/TCP,443:30135/TCP   2m

```

The External IP `104.198.24.38` is our route into the Deployment Pod `nginx-app-7c744547b7-ks5wv`
Local Test, SSH-less method - as per best practice considering security;

```
kubectl -n default exec -it nginx-app-7c744547b7-ks5wv -- bash
root@nginx-app-7c744547b7-ks5wv:/# apt-get update && apt-get install curl -y # Default nginx image comes sanitised to do one job only

root@nginx-app-7c744547b7-ks5wv:/# curl -I localhost/hello/
HTTP/1.1 200 OK
Server: nginx
Date: Thu, 07 Feb 2019 16:02:52 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 6
Last-Modified: Thu, 07 Feb 2019 16:00:29 GMT
Connection: keep-alive
ETag: "5c5c561d-6"
Accept-Ranges: bytes

root@nginx-app-7c744547b7-ks5wv:/# curl -Ik https://localhost/hello/
HTTP/2 200
server: nginx
date: Thu, 07 Feb 2019 16:03:09 GMT
content-type: text/html; charset=utf-8
content-length: 6
last-modified: Thu, 07 Feb 2019 16:00:29 GMT
etag: "5c5c561d-6"
accept-ranges: bytes

```

External IP Access
This can be listed with the following line, 
```
ubectl get svc nginx-app-svc-external -o wide
NAME                     TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)                      AGE       SELECTOR
nginx-app-svc-external   LoadBalancer   10.47.240.62   35.239.39.248   80:32575/TCP,443:31638/TCP   1m        run=nginx-app
```

And a quick whirl, to confirm this nginx-app can be accessed the very same way, externally;

```bash
curl  http://35.239.39.248/hello/
world

$ curl -I http://35.239.39.248/hello/
HTTP/1.1 200 OK
Server: nginx
Date: Thu, 07 Feb 2019 21:35:31 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 6
Last-Modified: Thu, 07 Feb 2019 21:34:34 GMT
Connection: keep-alive
ETag: "5c5ca46a-6"
Accept-Ranges: bytes

$ curl -Ik https://35.239.39.248/hello/
HTTP/2 200
server: nginx
date: Thu, 07 Feb 2019 21:35:39 GMT
content-type: text/html; charset=utf-8
content-length: 6
last-modified: Thu, 07 Feb 2019 21:34:34 GMT
etag: "5c5ca46a-6"
accept-ranges: bytes

$ curl -k https://35.239.39.248/hello/
world

```