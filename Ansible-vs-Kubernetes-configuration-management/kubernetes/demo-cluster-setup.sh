#!/usr/bin/env bash

gcloud beta container --project "jpworkspace" \
clusters create "playground-cluster" \
--zone "us-central1-c" \
--username "admin" \
--cluster-version "1.11.6-gke.6" \
--machine-type "g1-small" \
--image-type "COS" \
--disk-type "pd-standard" \
--disk-size "20" \
--scopes "https://www.googleapis.com/auth/devstorage.read_only",\
"https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring",\
"https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly",\
"https://www.googleapis.com/auth/trace.append" \
--num-nodes "1" \
--no-enable-cloud-logging \
--no-enable-cloud-monitoring \
--no-enable-ip-alias \
--network "projects/jpworkspace/global/networks/default" \
--subnetwork "projects/jpworkspace/regions/us-central1/subnetworks/default" \
--addons HorizontalPodAutoscaling,HttpLoadBalancing \
--enable-autoupgrade \
--metadata disable-legacy-endpoints=true \
--enable-autorepair
