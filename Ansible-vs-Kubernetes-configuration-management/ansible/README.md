
### Running Ansible

Once the VM is provisioned using the `setup-demovm.sh` gcloud command script,
we can confirm it's uptime and external IP address using the following;

```bash
gcloud compute instances list | grep demo-vm
demo-vm        us-east1-b     g1-small  10.142.0.3   35.227.31.78  RUNNING
```

So now, armed with IP address information, `35.227.31.78` we can commence the configuration process.


####Configuration 

This Deployment Component requires the ansible configuration run -  a playbook run;

```
ansible-playbook -i inventory/gcp/hosts.ini -l demovm -t nginx \
-e 'ansible_ssh_user=jaroslav sudo=yes sudo_user=root' \
--become-user=root --become-method=sudo -b main.yaml \
--vault-password-file ~/.ansible/password
```

Note: it expects my username `jaroslav` which is a pre-provisioned SSH key the `demovm` has been provisioned with.
Step removed for security purposes


#####Secrets: 
As you note, my ansible-vault contains the hashed privatekey for the demo role, 
and a temporary password file is referenced to decrypt ansibe vaults to enable successful read and execution of the playbook
The Vault can now be committed to repository, while keeping your password to that vault safely elsewhere


#### Expected Result

Once Ansible playbook deployment is completed successfully, you can give it a test, on localhost to start with.

```
curl -I localhost/hello/

HTTP/1.1 200 OK
Server: nginx
Date: Thu, 07 Feb 2019 12:58:13 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 5
Last-Modified: Thu, 07 Feb 2019 10:46:52 GMT
Connection: keep-alive
ETag: "5c5c0c9c-5"
Accept-Ranges: bytes

```

HTTPS component can also be tested, but since it's a self generated SSL cert, use option`-k` to skip SSL bundle verification

```
curl -k -I https://localhost/hello/

HTTP/1.1 200 OK
Server: nginx
Date: Thu, 07 Feb 2019 12:59:56 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 5
Last-Modified: Thu, 07 Feb 2019 10:46:52 GMT
Connection: keep-alive
ETag: "5c5c0c9c-5"
Accept-Ranges: bytes
```

#####External IP address
`35.227.31.78`, as per above instance information.

```bash

$ curl http://35.227.31.78/hello/
world

$ curl -I http://35.227.31.78/hello/
HTTP/1.1 200 OK
Server: nginx
Date: Thu, 07 Feb 2019 21:44:13 GMT
Content-Type: text/html; charset=utf-8
Content-Length: 5
Last-Modified: Thu, 07 Feb 2019 10:46:52 GMT
Connection: keep-alive
ETag: "5c5c0c9c-5"
Accept-Ranges: bytes

$ curl -Ik https://35.227.31.78/hello/
HTTP/2 200 
server: nginx
date: Thu, 07 Feb 2019 21:44:19 GMT
content-type: text/html; charset=utf-8
content-length: 5
last-modified: Thu, 07 Feb 2019 10:46:52 GMT
etag: "5c5c0c9c-5"
accept-ranges: bytes

$ curl -k https://35.227.31.78/hello/
world
```

#####Bonus
SSH user `demo` is created to enable access onto the VM for further management (sudo user)

Example:
```bash
$ ssh -i ~/.ssh/demo_id_rsa demo@35.227.31.78
[demo@demo-vm ~]$ uptime
 21:43:46 up 5 min,  1 user,  load average: 0.00, 0.02, 0.02
 ```


```