
echo "Provisioning the demo server vm"
gcloud compute --project=jpworkspace instances create demo-vm \
--description="demo server" --zone=us-east1-b \
--machine-type=g1-small \
--subnet=default \
--network-tier=PREMIUM \
--maintenance-policy=MIGRATE \
--service-account=596459254910-compute@developer.gserviceaccount.com \
--scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,\
https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,\
https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
--tags=demovm,http-server,https-server \
--image=centos-7-v20190116 \
--image-project=centos-cloud \
--boot-disk-size=10GB \
--boot-disk-type=pd-standard \
--boot-disk-device-name=demo-vm

echo "additional gcloud firewall setup for the instance. Ports, 80,443,22"
gcloud compute --project=jpworkspace firewall-rules create default-allow-http --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:80 --source-ranges=0.0.0.0/0 --target-tags=http-server
gcloud compute --project=jpworkspace firewall-rules create default-allow-https --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:443 --source-ranges=0.0.0.0/0 --target-tags=https-server
gcloud compute --project=jpworkspace firewall-rules create default-allow-https --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:22 --source-ranges=0.0.0.0/0 --target-tags=sshd

echo "Get Instance IP Information"
gcloud compute instances list