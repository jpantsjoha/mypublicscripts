** my Demo Repo with test script for personal projects

For Full Write up, visit link below 
[Typical Configuration Management (Ansible) on VM Versus The Kubernetes Application deployment with Yaml manifests](https://medium.com/@evda_uk/nginx-app-659901b04b05)

### What this is all about
The purpose of this demo is to showcase the difference of infrastructure configuration,
while deploying an nginx application, with hardening consideration.

Here are the two options;

- The new-age - kubernetes cluster + infrastructure as-code 

** Versus

- Classic Virtual Machine bulk with configuraiton management (with ansible)



### Docker & Kubernetes 
The demo sample in these two folders form one example 
Configuration resides in yaml manifests  in ./kubernetes location
Option to hack-in port 22 access. Dicouraged, unsupported, unrecommended (but if there is a will, there is a way)


#####Getting Started
Run the following
```demo-cluster-setup.sh``` to provision the basic one-node kubernetes cluster, within your cloud console

### Ansible (VM)
This method is Virtual Machine based
Configuration resides in ./ansible location
SSH accessible by default


#####Getting Started
Run the following
```setup-demovm.sh``` to provision the vanilla centos7 demovm, within your cloud console


### Overall Expected Outcome

Deploy a simple nginx service, serving content over 443
Have ssh access to the service
Security concious