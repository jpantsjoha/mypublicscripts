# How-to Kubernetes with DNS management (Use with Flux)

When it comes to GitOps, amongst the many caveats and the varied snags to watch out for when configuring the automated service provisioning, - one of these is the DNS. 
I have been long procrastinating to get a running demo of this External-Dns https://github.com/kubernetes-incubator/external-dns for a little while, alas it is here now.

External-DNS support DNS management, mapping **FQDN** to a service and an ingress. 
Albeit the Kubernetes Service DNS management will require a public IP address, provisioned with a `loadBalancer` type. While, this does make some aspect of automated provisioning simpler. DNS could be managed, records added and removed automatically.

![External-DNS operating model in K8](img/external-dns-operational-chart.png)

For most of the environments designs this would largely be the `development` and perhaps `Staging` environments could benefit from this **#GitOps CICD*, a version controlled, and automated process.

Let's assess the problem we’re solving - **Address the dynamic DNS mapping configuration requirement for our application**. 
This is may required for the, say Dev Team, when deploying and testing new application features. This tastes better within the *#GitOps* environment - where your Git Repo is all the `"source of truth"` which gets automatically provisioned.

I hope to **“Explain it, like to a 10 year old”** - with a reasonably straight forward demo.

Now, do bear with me, as I may have two audiences reading this post;
- I intend the hands-on number of you readers to be able to duplicate the success, providing you do have your own DNS in your Google Cloud DNS service mapped and configured. If so, by all means, do `git clone`, and copy-paste away at your heart’s content and like-share this along for virtual _beer kudos_.

- The rest reader-browser, I have screenshots with descriptions, which can help you orient the progress of this post.

### Your System Requirements
This may be obvious, but you will need `git`, `google-cloud-sdk` and `kubectl` installed on whichever system you are running this

Having had a go now setting this up. It is moderately straight forward with some expected K8 and GCP delays in service provisioning. When copy-paste testing yourself, bear that in mind.

### PreReqs & Sanity Checks

I expect you to have a DNS name managed by Google Cloud DNS, and Cloud DNS API enabled in the project you are replicating this run.
Furthermore, We will be setting up the DNS zone management first, and then continue with the fresh cluster setup. 
Follow the progress for details.

- My Test Domain `kseven.biz` - _(The same test domain I have used since my ComSci uni days at Kings College)_
- Ensure the correct NS servers are mapped to google Cloud DNS zone for `kseven.biz` zone. I mean it, check it, make sure they match. 


## The Process

### **Setting up GLOBAL VAR** ###

```
## This global will be used throughout the copy-paste scripts
## CHANGE THIS as required
PROJECT=evmeerkat-demo 
CLUSTER_NAME=evmeerkat-demo # im that creative
```

#### **0 - DNS** We provision the zone
Create a zone which we want external-DNS to manage - obvious benefit to have multiple cluster environments to manage own separate zones.
```
cloud dns managed-zones create "kseven-biz"  \
   --dns-name "kseven.biz."  \
   --description "Automatically managed zone by kubernetes.io/external-dns"

Created [https://dns.googleapis.com/dns/v1/projects/evmeerkat-demo/managedZones/kseven-biz].

```
#### **0.5 DNS Registrar update** 
Update the NS records with your registrar, having dropped TTL if possible earlier, or just wait the usual 24h until record update propagate.

Check again, to see them propagated correctly reflected in `dig` query.

```
dig NS kseven.biz
...
;; ANSWER SECTION:
kseven.biz.		21599	IN	NS	ns-cloud-d1.googledomains.com.
kseven.biz.		21599	IN	NS	ns-cloud-d2.googledomains.com.
...
```


#### **1 DNS - Additional Zone & Verification** 
Default view should contain about 2 records
```
jp$ gcloud dns record-sets list     --zone "kseven-biz" 
NAME         TYPE  TTL    DATA
kseven.biz.  NS    21600  ns-cloud-d1.googledomains.com.,ns-cloud-d2.googledomains.com.,ns-cloud-d3.googledomains.com.,ns-cloud-d4.googledomains.com.
kseven.biz.  SOA   21600  ns-cloud-d1.googledomains.com. cloud-dns-hostmaster.google.com. 1 21600 3600 259200 300
```

**Note:** if you have a sub zone within the main zone, you need to tell the root zone where to find these records as well. 

Tell the parent zone where to find the DNS records for this zone by adding the corresponding NS records there. Assuming the new zone is "evmeerkat-demo" and the domain is "kseven-biz" and that it's also hosted at Google we would do the following;

```
# Setting up GLOBAL VAR
## This global will be used throughout the copy-paste scripts
## CHANGE THIS as required
$ PROJECT=evmeerkat-demo 

$ gcloud dns record-sets transaction start --zone "$PROJECT"
$ gcloud dns record-sets transaction add ns-cloud-e{1..4}.googledomains.com. \
    --name "kseven.biz." --ttl 300 --type NS --zone $PROJECT"
$ gcloud dns record-sets transaction execute --zone "$PROJECT"
```


#### **2 Demo Cluster Setup** 
Creating a default cluster-demo cluster.
Pay attention to the scope permissions your potentially current Cluster, Node-Pool comes with - Requires CloudDNS RW - if you don’t have that, you will be forced to create a new node-pool. Good luck.

But, for the simplicity of the demo, I allow all permission scopes, but you should cut-down permissions to "Least privileged requirements" tune to your needs.

`A basic, default spec 2 node cluster with preemptible nodes`
```
gcloud beta container --project "$PROJECT" clusters create "$CLUSTER_NAME" \
--zone "us-central1-a" --no-enable-basic-auth \
--cluster-version "1.14.6-gke.2" --machine-type "n1-standard-2" \
--image-type "COS" --disk-type "pd-ssd" --disk-size "50" \
--metadata disable-legacy-endpoints=true \
--scopes "https://www.googleapis.com/auth/cloud-platform" \
--preemptible --num-nodes "2" \
--enable-cloud-logging \
--enable-stackdriver-kubernetes --enable-ip-alias \
--network "projects/$PROJECT/global/networks/default" \
--subnetwork "projects/$PROJECT/regions/us-central1/subnetworks/default" \
--default-max-pods-per-node "110" --addons HorizontalPodAutoscaling,HttpLoadBalancing \
--enable-autoupgrade \
--enable-autorepair
```

Example Created Cluster
```
Creating cluster evmeerkat-demo in us-central1-a... Cluster is being deployed...⠼

kubeconfig entry generated for evmeerkat-demo.
NAME            LOCATION       MASTER_VERSION  MASTER_IP       MACHINE_TYPE   NODE_VERSION  NUM_NODES  STATUS
evmeerkat-demo  us-central1-a  1.13.7-gke.8    104.198.146.83  n1-standard-1  1.13.7-gke.8  2          RUNNING
```

#### **3 Connect to Cluster & verify** 
Configuring local environment to use the correct project and obtain cluster credentials to log with.

```
gcloud config set project $PROJECT
gcloud container clusters get-credentials demo-cluster --zone us-central1-a --project $PROJECT
```

```
kubeconfig entry generated for evmeerkat-demo.
NAME            LOCATION       MASTER_VERSION  MASTER_IP     MACHINE_TYPE   NODE_VERSION  NUM_NODES  STATUS
evmeerkat-demo  us-central1-a  1.13.7-gke.8    35.232.174.0  n1-standard-1  1.13.7-gke.8  2          RUNNING
```

#### **4 Ready to Git** 
If you reading this blog elsewhere, you can still checkout the demo code base if you have not already and start applying the YAMLs

`git clone https://jpantsjoha@bitbucket.org/jpantsjoha/mypublicscripts.git && cd mypublicscripts/external-dns-managed-k8-app-service/`


#### **5 Apply the YAML configuration manifests** 
The list of the YAML definitions you're about to apply and enable in your Kubernetes cluster.

```
jp$ ls
README.md                               nginx-demo-svc-kseven-biz-ingress.yaml  nginx-kseven-biz-deployment.yaml
external-dns-withRBAC.yaml              nginx-demo-svc-kseven-biz-service.yaml
```

All in order?

**Let's do this!**

`jp$ kubectl apply -f . `
```
serviceaccount/external-dns created
clusterrole.rbac.authorization.k8s.io/external-dns created
clusterrolebinding.rbac.authorization.k8s.io/external-dns-viewer created
deployment.extensions/external-dns created
ingress.extensions/nginx-demo-ing-kseven-biz created
service/nginx-demo-svc-kseven-biz created
deployment.extensions/nginx-kseven-biz created
```

These include the provisioning of `nginx` application in a form of `'Deployment(rs 1) -> Service -> Ingress'`, as well as the `external-dns-withRBAC.yaml` which contains the external-dns `Deployment`, `ClusterRole` and `ClusterRoleBindings` permission mappings.


Let’s see the result of the aforementioned 'Apply All' on the cluster.

````
jp$ kubectl get po,svc,ing
NAME                                   READY   STATUS    RESTARTS   AGE
pod/external-dns-64df64799c-vt5sx      1/1     Running   0          42s
pod/nginx-kseven-biz-cbfd99bf6-4rhf4   1/1     Running   0          41s

NAME                                TYPE           CLUSTER-IP   EXTERNAL-IP   PORT(S)        AGE
service/kubernetes                  ClusterIP      10.0.0.1     <none>        443/TCP        13m
service/nginx-demo-svc-kseven-biz   LoadBalancer   10.0.12.69   35.226.173.177     80:31002/TCP   41s

NAME                                           HOSTS                  ADDRESS   PORTS   AGE
ingress.extensions/nginx-demo-ing-kseven-biz   via-nginx.kseven.biz             80      41s
````
Both Service and Ingress come with a provisioned Public IP addresses `35.226.173.177` and `34.102.132.127`

#### **6 Test Service and Ingress access** 
First, checking the `nginx application` is working BAU, and we can jump straight to the `service` and use the **External IP** to verify nginx is A-Ok.

```
jp$ curl 35.226.173.177
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
...
```

Now, Let's confirm the `external-dns` application is working, and logs are healthy.

`jp$ kubectl logs -f external-dns-64df64799c-vt5sx`
```
...
time="2019-09-16T20:47:16Z" level=info msg="Change zone: kseven-biz"
time="2019-09-16T20:47:16Z" level=info msg="Google project auto-detected: evmeerkat-demo"
time="2019-09-16T20:47:16Z" level=info msg="All records are already up to date"
time="2019-09-16T20:47:12Z" level=info msg="Change zone: kseven-biz"
time="2019-09-16T20:47:12Z" level=info msg="Add records: nginx.kseven.biz. A [35.226.173.177] 300"
time="2019-09-16T20:47:12Z" level=info msg="Add records: via-nginx.kseven.biz. A [34.102.132.127] 300"
time="2019-09-16T20:47:12Z" level=info msg="Add records: nginx.kseven.biz. TXT [\"heritage=external-dns,external-dns/owner=my-kseven-biz-name,external-dns/resource=service/default/nginx-demo-svc-kseven-biz\"] 300"
time="2019-09-16T20:49:14Z" level=info msg="Add records: via-nginx.kseven.biz. TXT [\"heritage=external-dns,external-dns/owner=my-kseven-biz-name,external-dns/resource=ingress/default/nginx-demo-ing-kseven-biz\"] 300"
time="2019-09-16T20:57:46Z" level=info msg="All records are already up to date"
....
```


#### **7 Confirm the Google Cloud DNS zone records**

```
jp$ gcloud dns record-sets list     --zone "kseven-biz"  
NAME                   TYPE  TTL    DATA
kseven.biz.            NS    21600  ns-cloud-d1.googledomains.com.,ns-cloud-d2.googledomains.com.,ns-cloud-d3.googledomains.com.,ns-cloud-d4.googledomains.com.
kseven.biz.            SOA   21600  ns-cloud-d1.googledomains.com. cloud-dns-hostmaster.google.com. 1 21600 3600 259200 300
nginx.kseven.biz.      A     300    35.226.173.177
nginx.kseven.biz.      TXT   300    "heritage=external-dns,external-dns/owner=my-kseven-biz-name,external-dns/resource=service/default/nginx-demo-svc-kseven-biz"
via-nginx.kseven.biz.  A     300    34.102.132.127
via-nginx.kseven.biz.  TXT   300    "heritage=external-dns,external-dns/owner=my-kseven-biz-name,external-dns/resource=ingress/default/nginx-demo-ing-kseven-biz"
```

**Looks legit.** 
- Nginx application is deployed. 
- service is configured. 
- Ingress is configured.
- Dns records for each of the above are now in the `kseven.biz` zone as intended.


#### **7 Testing DNS record mapping**

**So does it work(?)**
Does, Like magic.

Test `Service` DNS mapping to `nginx.kseven.biz`

`dig` query first, for sanity.
```
jp$ dig A nginx.kseven.biz
...

;; ANSWER SECTION:
nginx.kseven.biz.	24	IN	A	35.226.173.177
```

`curl`-test the nginx application, should be working BAU.

```
# IP
jp$ curl 104.154.164.87
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
...

# DNS
jp$ curl nginx.kseven.biz
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
...
```

**Note:** Ingress usually takes a bit longer to setup due to the nature of the controller residing within the GCE environment in GCP. 

Test `Ingress` now,  DNS mapping to `via-nginx.kseven.biz`

`dig` query first, for sanity.
```
jp$ dig A via-nginx.kseven.biz
...

;; ANSWER SECTION:
via-nginx.kseven.biz.	211	IN	A	34.102.132.127
```

`curl`-test the nginx application, should be working BAU.

```
jp$ curl http://via-nginx.kseven.biz/
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
...
```

## **8 Result**
Good stuff. `External-DNS` does what it says on the tin. 

Notes: Your Services require a public ip (separate cost) and specified Service `Annotations`.

```
  annotations:
    external-dns.alpha.kubernetes.io/hostname: nginx.kseven.biz.
```

`External-DNS` container arguments worth to mention again.
```
args:
- --source=service
- --source=ingress
# - --domain-filter=kseven.biz # will make ExternalDNS see only the hosted zones matching provided domain, omit to process all available hosted zones
- --provider=google
# - --google-project=evmeerkat-demo # Use this to specify a project different from the one external-dns is running inside
# - --policy=upsert-only # would prevent ExternalDNS from deleting any records, omit to enable full synchronization
- --registry=txt
- --txt-owner-id=my-kseven-biz-name
```

## **9 Cleanup**
Nuking cluster. Be careful not to nuke anything else by accident.
_"Don't do anything I would do"_
```
jp$ gcloud container clusters list
NAME            LOCATION       MASTER_VERSION  MASTER_IP     MACHINE_TYPE   NODE_VERSION  NUM_NODES  STATUS
evmeerkat-demo  us-central1-a  1.13.7-gke.8    35.232.174.0  n1-standard-1  1.13.7-gke.8  2          RUNNING

jp$ gcloud container clusters delete evmeerkat-demo --zone us-central1-a
The following clusters will be deleted.
 - [evmeerkat-demo] in [us-central1-a]

Do you want to continue (Y/n)?  y

Deleting cluster evmeerkat-demo...⠶  
```





## **10 Hope you enjoyed the demo**
 
Give a like, a share and some good vibes, if you found it useful

*Until Next time,*

*Jaroslav Pantsjoha*