## External-DNS Demo Prep ##
This deployment is automating the DNS management within the GCP, 
however you do need to tell your deployment which Project and the Domain to take under management

**Update Deployment Container Arguments**

The one two main pieces for you to tune, for demo purposes are as per following working example;

```args:```

`--google-project=evmeerkat-demo`

`--domain-filter=kseven.biz`
